
!function() {
	$(() => {
		let state = JSON.parse(localStorage.getItem('state') || '[]');

		const $modal = $('#create-task-modal');
		const $list = $('#list');

		const showModal = () => {
			const $input = $modal.find('.content input[type="text"]');
				$input.val('');
				$input.trigger('focus');
			$modal.fadeIn('fast');
		};
		const hideModal = () => $modal.fadeOut('fast');
		const addTask = () => {
			hideModal();
			state.push({
				name: $modal.find('.content input[type="text"]').val(),
				status: 'new',
			});
			send();
			update();
		};

		const send = () => {
			localStorage.setItem('state',JSON.stringify(state));
			console.log(state)
		};


		const update = (() => {
			let lastState = [];

			// language=HTML
			const createNode = num =>
				`<div data-num = "${num}">
					<input data-num = "${num}" type = "text">
					<div>
						<select data-num = "${num}">
							<option value = "new">new</option>
							<option value = "in progress">in progress</option>
							<option value = "ready for test">ready for test</option>
							<option value = "resolve">resolve</option>
						</select>
						<button data-num = "${num}">⮾</button>
					</div>
				</div>`;

			const change = {
				name: i => $list.find(`div[data-num="${i}"]`).find('input[type="text"]').val(state[i].name),
				status: i => $list.find(`div[data-num="${i}"]`).find('select').val(state[i].status),
			};

			return hard => {
				const {length} = state;
				const {length: lastLength} = lastState;

				let i;

				if (length > lastLength)
				{
					for (i = lastLength; i < length; i++)
					{
						$list.prepend(createNode(i));
						change.name(i);
						change.status(i);
					}
				}
				else if (length < lastLength)
				{
					for (i = length; i < lastLength; i++)
						$list.find(`div[data-num="${i}"]`).remove();
				}

				const keys = Object.keys(change);
				for (i = 0; i < Math.min(length,lastLength); i++)
					for (let j = 0; j < keys.length; j++)
					{
						const key = keys[j];
						if (hard || lastState[i][key] !== state[i][key])
							change[key](i);
					}

				lastState = state.map(item => ({...item}));
			}

		})();

		$modal.find('.actions > button:last-child').click(hideModal);
		$modal.find('.actions > button:first-child').click(addTask);
		$modal.find('.dimmer').click(hideModal);
		$modal.find('.content').click(e => e.stopPropagation());
		$('#tasks-label').find('button').click(showModal);

		$list.on('click','div > div > button',function () {
			const num = parseInt($(this).data('num'));
			state.splice(num,1);
			send();
			update();
		});

		$list.on('blur','div > input',function () {
			const num = parseInt($(this).data('num'));
			const value = $(this).val();
			if (state[num].name !== value);
			{
				state[num].name = value;
				send();
				update();
			}
		});

		$list.on('change','div > div > select',function () {
			const num = parseInt($(this).data('num'));
			const value = $(this).val();
			if (state[num].status !== value);
			{
				state[num].status = value;
				send();
				update();
			}
		});

		update();

	});
}();